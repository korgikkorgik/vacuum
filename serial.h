#ifndef SERIAL_H
#define SERIAL_H
#include <QApplication>
#include <QSerialPort>

//#define PARSING_DATA_MAX_SIZE 10

class Serial
{
public:
    Serial();
    ~Serial();

    qint32 open(QString name);
    void close();
    qint32 write(QByteArray data);
    qint32 getPacket();

    struct Settings {
        QString name;
        qint32 baudRate;
        QString stringBaudRate;
        QSerialPort::DataBits dataBits;
        QString stringDataBits;
        QSerialPort::Parity parity;
        QString stringParity;
        QSerialPort::StopBits stopBits;
        QString stringStopBits;
        QSerialPort::FlowControl flowControl;
    };
    Settings settings;

    enum State{disconnected,connected};
    State state;

    QString packetPdu;

private:
    void defaultSettings();
    void getPacketInit();


private:
    QSerialPort *pSerialPort;

    enum PacketState{packetWait,packetStart};
    PacketState packetState;

    const qint32 packetMaxSize = 32;
    const qint32 packetLimitRead = 64;




};

#endif // SERIAL_H
