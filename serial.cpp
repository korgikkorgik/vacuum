#include "serial.h"
#include "command.h"

Serial::Serial()
{
    pSerialPort = new QSerialPort();
    state = State::disconnected;

    getPacketInit();
}

Serial::~Serial()
{
    delete pSerialPort;
    defaultSettings();
}

void Serial::defaultSettings()
{
    //settings.baudRate = QSerialPort::Baud1200;
    settings.baudRate = QSerialPort::Baud115200;
    settings.stringBaudRate = "115200";
    settings.dataBits = QSerialPort::Data8;
    settings.stringDataBits = "8";
    settings.parity = QSerialPort::NoParity;
    settings.stringParity = "N";
    settings.stopBits = QSerialPort::OneStop;
    settings.stringStopBits = "1";
    settings.flowControl = QSerialPort::NoFlowControl;
}

// ********************************************************************
// Открытие последовательного порта с заданным именем
// Возвращает 1/0 в случае успеха/ошибки
// ********************************************************************
qint32 Serial::open(QString name)
{
    defaultSettings();
    settings.name = name;

    pSerialPort->setPortName(settings.name);
    pSerialPort->setBaudRate(settings.baudRate);
    pSerialPort->setDataBits(settings.dataBits);
    pSerialPort->setParity(settings.parity);
    pSerialPort->setStopBits(settings.stopBits);
    pSerialPort->setFlowControl(settings.flowControl);

    if (pSerialPort->open(QIODevice::ReadWrite)) {
        pSerialPort->clear();
        state = State::connected;
        return 1;
    } else return 0;
}

// ********************************************************************
// Закрытие последовательного порта
// ********************************************************************
void Serial::close()
{
    if (pSerialPort->isOpen()) pSerialPort->close();
    state = State::disconnected;
}

// ********************************************************************
// Запись данных в открытый последовательный порт
// Возвращает 1 в случае успеха
// ********************************************************************
qint32 Serial::write(QByteArray data)
{
    if (data.size() == pSerialPort->write(data)) return 1; else return 0;
}

// ********************************************************************
//
// ********************************************************************
void Serial::getPacketInit()
{
    packetState = PacketState::packetWait;
}


// ********************************************************************
// Выделение пакета из входящего потока данных
// Пакетом считаются данные начиная с первого символа после '\r' или '\n'
// и ограничанные символом '\r' или '\n'
// Возвращает 1 в случае успеха
// ********************************************************************
qint32 Serial::getPacket()
{
    char data;
    qint32 numRead = 0;

    switch (packetState) {

    case PacketState::packetWait:
        //packet.pdu = "";
        packetPdu.clear();

        while (1){
            if (numRead >= packetLimitRead) return 0;
            if (!pSerialPort->bytesAvailable()) return 0;

            pSerialPort->read(&data,1);
            numRead ++;

            if ((data != PACKET_END_1) && (data != PACKET_END_2)){
                packetPdu += data;
                packetState = PacketState::packetStart;
                break;
            }
        }
    goto m1;
    //break;

    m1:
    case PacketState::packetStart:
        while (1){
            if (numRead >= packetLimitRead) return 0;
            if (!pSerialPort->bytesAvailable()) return 0;

            pSerialPort->read(&data,1);
            numRead ++;

            if ((data == PACKET_END_1) || (data == PACKET_END_2)){
                packetState = PacketState::packetWait;
                return 1;
            }

            packetPdu += data;
            if (packetPdu.size() > packetMaxSize){
                packetState = PacketState::packetWait;
                return 0;
            }
        }
    }
    return 0;
}
