#ifndef PARSING_H
#define PARSING_H
#include <QApplication>

#define PARSING_DATA_MAX_SIZE 10

class Parsing
{
public:
    Parsing();
    qint32 getCameraState(QString &pdu);

    struct CameraState {
        QString pressure;
        QString door;
        QString cup_1_state;
        QString cup_1_pos;
        QString cup_2_state;
        QString cup_2_pos;
    };
    CameraState cameraState;
};

#endif // PARSING_H
