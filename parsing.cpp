#include "parsing.h"
#include "mainwindow.h"
#include "serial.h"
#include "command.h"

Parsing::Parsing()
{

}

// ********************************************************************
// Разбор ответа на запрос состояния вида
// ? <pressure> <door> <cup 1 state> <cup 1 pos> <cup 2 state> <cup 2 pos>
// Возвращает 1/0 в случае успеха/ошибки
// ********************************************************************
qint32 Parsing::getCameraState(QString &pdu)
{
    if (pdu[0] != CAMERA_STATE_PREFIX) return 0;
    pdu.remove(0,2); // Удаляем первые два символа

    // Разделяем данные на список по разделителю - "пробел"
    QStringList list = pdu.split(" ");

    // Проверка количества полученных записей
    //if (list.size() != 5) return 0;

    // Раскладываем данные в структуру
    cameraState.pressure = list[0];
    cameraState.door = list[1];
    cameraState.cup_1_state = list[2];
    cameraState.cup_1_pos = list[3];
    cameraState.cup_2_state = list[4];
    cameraState.cup_2_pos = list[5];

    return 1;
}
