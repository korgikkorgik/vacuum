#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QSerialPortInfo>
#include <QMessageBox>
#include "command.h"
#include "parsing.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    serial = new Serial;

    QTimer *timer = new QTimer(this);
    connect(timer,&QTimer::timeout,this,&MainWindow::timeTask);
    timer->start(100);

    fillPortsInfo();

    linkTimerInit();

    ui->lcdNumber->setDecMode();
    int a = 100;
    ui->lcdNumber->setSegmentStyle(QLCDNumber::SegmentStyle::Flat);
    ui->lcdNumber->display(a);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete serial;
}

void MainWindow::fillPortsInfo()
{
    ui->serialPortListComboBox->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        list << info.portName();
        ui->serialPortListComboBox->addItem(list.first(), list);
    }
}

// ********************************************************************
// Обработка linkTimer
// ********************************************************************
void MainWindow::linkTimerInit()
{
    linkTimer  = 0;
}

void MainWindow::linkTimerHandler()
{
    if (linkTimer != 0) linkTimer --;
}

qint32 MainWindow::linkTimerState()
{
    return linkTimer;
}

void MainWindow::linkTimerRefresh()
{
    linkTimer = LINK_TIMER_DELAY;
}

// ********************************************************************
// Слот кнопки открытия/закрытия порта
// ********************************************************************
void MainWindow::on_serialPortListPushButton_clicked()
{
    switch (serial->state) {
        case Serial::State::disconnected:
            if(serial->open(ui->serialPortListComboBox->currentText())){
                ui->serialPortListComboBox->setEnabled(false); // Деактивируем элемент выбор порта
                ui->serialPortListPushButton->setText("Disconnect"); // Меняем текст на кнопке
                ui->statusBar->showMessage(tr("Connected to %1 : %2, %3, %4, %5")
                                  .arg(serial->settings.name)
                                  .arg(serial->settings.stringBaudRate)
                                  .arg(serial->settings.stringDataBits)
                                  .arg(serial->settings.stringParity)
                                  .arg(serial->settings.stringStopBits));
            } else ui->statusBar->showMessage("Open error");
        break;

        case Serial::State::connected:
            serial->close();
            ui->serialPortListComboBox->setEnabled(true); // Активируем элемент
            ui->serialPortListPushButton->setText("Connect"); // Меняем текст на кнопке
            ui->statusBar->showMessage(tr("Disconnected"));
        break;
    }
}

// ********************************************************************
// Слот для обработки прерываний по времени
// ********************************************************************
void MainWindow::timeTask()
{
    linkTimerHandler();

    // Индикация состояния связи
    if (linkTimerState())
    {
        ui->linkLabel->setText("Связь установлена");
        ui->linkLabel->setStyleSheet("QLabel{color:green}");
    }else{
        ui->linkLabel->setText("Нет связи");
        ui->linkLabel->setStyleSheet("QLabel{color:red}");
    }

    // Проверка режима
    if (serial->state ==  Serial::State::disconnected) return;


    // Ожидание пакета
    if (!serial->getPacket()) return;
    //linkTimerRefresh();

    // Парсинг пакета
    Parsing parsing;
    if (!parsing.getCameraState(serial->packetPdu)) return;
    linkTimerRefresh();


    // Обработка результатов

    bool ok;
    qint32 pressure = parsing.cameraState.pressure.toInt(&ok,10);
    if (!ok) return;
    ui->lcdNumber->display(pressure);


}
// ********************************************************************
// Слоты для панели чаши 1
// ********************************************************************
void MainWindow::on_cup_1_home_pushButton_clicked()
{
    serial->write(CMD_CUP_1_HOME);
}

void MainWindow::on_cup_1_up_pushButton_pressed()
{
    serial->write(CMD_CUP_1_UP);
}

void MainWindow::on_cup_1_up_pushButton_released()
{
    serial->write(CMD_CUP_1_STOP);
}

void MainWindow::on_cup_1_down_pushButton_pressed()
{
    serial->write(CMD_CUP_1_DOWN);
}

void MainWindow::on_cup_1_down_pushButton_released()
{
    serial->write(CMD_CUP_1_STOP);
}

// TEST
void MainWindow::on_testPushButton_clicked()
{
    // ? <pressure> <door> <cup 1 state> <cup 1 pos> <cup 2 state> <cup 2 pos>
    serial->write("? 523 c m 123 m 234 \r\n");
}
