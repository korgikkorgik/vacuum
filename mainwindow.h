#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serial.h"

#define LINK_TIMER_DELAY 20;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public:
    //Serial *serial;


private slots:
    void on_serialPortListPushButton_clicked();
    void timeTask();
    void on_cup_1_up_pushButton_pressed();
    void on_cup_1_up_pushButton_released();
    void on_cup_1_home_pushButton_clicked();
    void on_cup_1_down_pushButton_pressed();
    void on_cup_1_down_pushButton_released();

    void on_testPushButton_clicked();

private:
    Ui::MainWindow *ui;
    Serial *serial;
    qint32 linkTimer;

private:
    void fillPortsInfo();

    void linkTimerInit();
    void linkTimerHandler();
    void linkTimerRefresh();
    qint32 linkTimerState();


};

#endif // MAINWINDOW_H
